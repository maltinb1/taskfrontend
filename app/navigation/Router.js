import React, {Component} from 'react';
import { Scene, Router, Actions,Tabs} from 'react-native-router-flux';

import Home from '../components/Home';
import Forgotten from '../components/Forgotten';
import Cars from '../components/Cars';
import CarEdit from '../components/CarEdit';
import AddCar from '../components/AddCar';
import EngineType from '../components/EngineType';
import Manufacturer from '../components/Manufacturer';
import AddManufacturer from '../components/AddManufacturer';
import Settings from '../components/Settings';
import PasswordChange from '../components/PasswordChange';
import CarDetail from '../components/CarDetail';
import DamageDetail from '../components/DamageDetail';

import * as taskActions from '../actions/taskActions';
import { connect } from 'react-redux';

class RouterComponent extends Component {

    constructor(props) {
        super(props);
       
      }
   
    render(){
      console.log('token',this.props.token)
    return(

        <Router>
            
            <Scene key="opening" hideNavBar> 


              {this.props.token=="" ? <Scene key="Home" duration={1}  component={Home} hideNavBar={true}/>: <Scene key="Cars" duration={1} component={Cars} hideNavBar={true} /> }                                          
              <Scene key="CarDetail" duration={1}  component={CarDetail} hideNavBar={true}/>
              <Scene key="PasswordChange" duration={1}  component={PasswordChange} hideNavBar={true} />
              <Scene key="Settings" duration={1}  component={Settings} hideNavBar={true} />
              <Scene key="AddManufacturer" duration={1}  component={AddManufacturer} hideNavBar={true} />
              <Scene key="Manufacturer" duration={1}  component={Manufacturer} hideNavBar={true} />
              <Scene key="EngineType" duration={1}  component={EngineType} hideNavBar={true} />
              <Scene key="Home" duration={1}  component={Home} hideNavBar={true}/>
              <Scene key="Cars" tabs={true} tabBarStyle={{top:0}}    component={Cars} hideNavBar={true} /> 
              <Scene key="Forgotten"   component={Forgotten} hideNavBar={true}/>  
              <Scene key="CarEdit" component={CarEdit} hideNavBar={true} />
              <Scene key="AddCar"  component={AddCar} hideNavBar={true} />
              <Scene key="DamageDetail" duration={1}  component={DamageDetail} hideNavBar={true}/>

                
             
            </Scene>
        </Router>
    );
                }
};

function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
        // budgetCreate: state.BudgetCreateReducer.budgetCreate
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      addToken: (token) => dispatch(taskActions.addToken(token)),
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(RouterComponent);

