import * as types from './types';

export function addToken(token){
    return{
        type: types.ADD_TOKEN,
        token
    };
}
export function deleteToken(token){
    return{
        type: types.DELETE_TOKEN,
        token
    };
}


