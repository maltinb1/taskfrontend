/**
 * React Native App
 * Everthing starts from the entrypoint
 */
import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import Router from './navigation/Router';
import configureStore from 'app/store/configureStore';
const { persistor, store } = configureStore();
import codePush from "react-native-code-push";


export default class Entrypoint extends Component {
    constructor(props){
		super(props);
		this.state = {}
	}

    componentDidMount(){
        codePush.sync({ deploymentKey: "qVqY_EloaU_0rz0VvaZi3OhJdKF755qkmh-_0"});
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate
                    loading={<ActivityIndicator />}
                    persistor={persistor}
                >
                    <Router/>
                </PersistGate>
            </Provider>
        );
    }
}
