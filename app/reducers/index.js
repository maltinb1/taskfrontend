/* 
 * combines all th existing reducers
 */
import * as loadingReducer from './loadingReducer';
import * as loginReducer from './loginReducer';
import * as TaskReducer from './TaskReducer';

export default Object.assign(loginReducer, loadingReducer,TaskReducer);
