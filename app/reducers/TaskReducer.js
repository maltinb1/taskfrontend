import createReducer from 'app/lib/createReducer';
import * as types from 'app/actions/types';
import { getActiveChildNavigationOptions } from 'react-navigation';

const initialState = {
  
   token: '',
   cars:[],
   dieselCars:[],
   gasCars:[],
    
    
};

export const TaskReducer = createReducer(initialState,{

  [types.ADD_TOKEN](state, action){
    return {
        ...state,
        token: action.token
    };
},

[types.DELETE_TOKEN](state, action){
  return {
      ...state,
      token: action.token
  };
},

});