import React , { Component } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
    ActivityIndicator
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import ColorPalette from 'react-native-color-palette'


var isHidden = true;


class CarEdit extends Component{
    constructor(props) {
		super(props);
		this.state = {
           price: this.props.price,
           color: this.props.color,
           model: this.props.model,
           kilometer: this.props.kilometer,
           year: this.props.year,
           updateId: this.props.updateId,
           loading: false
		}
    }

    

     


    isBlank = ()=>{
        if (this.state.price=='' || this.state.color==''  || this.state.model=='' || this.state.year==''  || this.state.kilometer=='') {
              return(
                <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                    <Text style={{fontSize:18,fontWeight:'400', paddingRight:10,opacity:0.3}}>Save</Text> 
                </View>
              )
        }
        else{
            return (
                <TouchableOpacity activeOpacity={1} style={{flex:1,justifyContent: "center",alignItems: "flex-end"}} >
                    <Text style={{fontSize:18,fontWeight:'400', paddingRight:10}} onPress={()=>{this.UpdateItem();  Actions.Cars(); }}>Save</Text> 
                </TouchableOpacity>
            )
        }
    }



    UpdateItem = async()=>{
        let token = 'Token '+this.props.token
        this.setState({loading:true})
        try {
          
            fetch('http://54.203.20.237:5000/Cars/CarApi/'+this.state.updateId+'/', {
                method: 'PUT',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
                },
            body: JSON.stringify({
                price: this.state.price,
                color: this.state.color,
                model: this.state.model,
                currentKm: this.state.kilometer,
                yearOfFabrication: this.state.year,
                id: this.state.updateId
            }),
                
            }).then((response) => response.json())
                .then((responseJson) => {
                    // console.log('response',responseJson);
                   
                })
                .catch((error) => {
                    
                    console.error(error);
                });
            }catch(err) {
                console.warn(err); 
    }
    this.setState({loading:false})

    }



    render(){ 

       
        // console.log('STATE',this.state)
        // console.log('props',this.props)
    
        const {width,height} = Dimensions.get('window');
        return(
            <View style={{flex:1,backgroundColor:'#f5f5f5'}}>
                <View style={{flex:0.13,paddingTop:30, backgroundColor: "#ff6c5c",alignItems:'center',justifyContent:'center' }}>
                    
                    <View style={{flexDirection: 'row',  width: width}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} onPress={Actions.pop}>
                            <Text  style={{paddingLeft: 10,fontSize:18}}>Cancel</Text> 
                        </TouchableOpacity>

                        {/* <Text style={{fontSize:24,fontWeight:'600'}}>{budgets[editIndex].name}</Text> */}
                        <Text style={{fontSize:24,fontWeight:'600'}}> Update </Text>


                        
                            {this.isBlank()}
                            {/* <Text style={{fontSize:18,fontWeight:'400', paddingRight:10}}  onPress={()=> this.props.navigation.navigate('IconSelect')}> Save</Text> */}
                        

                    </View>
                </View>

                <View style={styles.topStyle}>
            
                    
                    

                    
                {this.state.loading ?  <ActivityIndicator color="black" size="large"/> : <View></View>}

                    

                    <View style={styles.monthSpendings}>
                        <View style={{padding:5,flex:1,flexDirection:'row',alignItems:'center',padding:6}}>
                                <FontAwesome name="money" style={{fontSize:25,color:'turquoise',width:33}}/>
                                <Text style={{color:'#ff6c5c', fontWeight:'bold'}}>Price :</Text>
                        </View>
                        <View  style={{flexDirection:'row' ,justifyContent:'flex-end', alignItems:'flex-end'}}>  
                            <TextInput                      
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {(this.state.price).toString()}
                                    keyboardType={'numeric'}
                                    onChangeText = {(price) => this.setState({price})}
                                    multiline={false}
                                />

                        </View> 

                    </View>

                    <View
                        style={{
                            borderBottomColor: 'black',
                            borderBottomWidth: 1,
                            opacity: 0.1,
                            paddingLeft:17,
                            paddingRight:17,
                            
                        }}
                    /> 

                    <View style={styles.monthSpendings}>
                        <TouchableOpacity style={{padding:5,flex:1,flexDirection:'row',alignItems:'center'}} >
                            <Ionicons name="ios-color-palette" style={{fontSize:25,color:'turquoise',width:33}}/>
                            <Text style={{color: '#ff6c5c', fontWeight:'bold'}} >Color :</Text>
                        </TouchableOpacity>

                        <View  style={{flexDirection:'row' ,justifyContent:'flex-end', alignItems:'flex-end'}}>  
                            <TextInput 
                                  
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {this.state.color}
                                    onChangeText = {(color) => this.setState({color})}
                                    multiline={false}
                                />

                        </View> 

                        
                    </View>

                    <View
                        style={{
                            borderBottomColor: 'black',
                            borderBottomWidth: 1,
                            opacity: 0.1,
                            paddingLeft:17,
                            paddingRight:17,
                            
                        }}
                    /> 

                    <View style={styles.monthSpendings}>
                        <TouchableOpacity style={{padding:5,flex:1,flexDirection:'row',alignItems:'center'}} >
                            <Ionicons name="ios-color-palette" style={{fontSize:25,color:'turquoise',width:33}}/>
                            <Text style={{color: '#ff6c5c', fontWeight:'bold'}} >Model :</Text>
                        </TouchableOpacity>

                        <View  style={{flexDirection:'row' ,justifyContent:'flex-end', alignItems:'flex-end'}}>  
                            <TextInput 
                                    
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {this.state.model}
                                    onChangeText = {(model) => this.setState({model})}
                                    multiline={false}
                                />

                        </View> 

                        
                    </View>

                    <View
                        style={{
                            borderBottomColor: 'black',
                            borderBottomWidth: 1,
                            opacity: 0.1,
                            paddingLeft:17,
                            paddingRight:17,
                            
                        }}
                    /> 

                    <View style={styles.monthSpendings}>
                        <TouchableOpacity style={{padding:5,flex:1,flexDirection:'row',alignItems:'center'}} >
                            <Ionicons name="ios-color-palette" style={{fontSize:25,color:'turquoise',width:33}}/>
                            <Text style={{color: '#ff6c5c', fontWeight:'bold'}} >Year :</Text>
                        </TouchableOpacity>

                        <View  style={{flexDirection:'row' ,justifyContent:'flex-end', alignItems:'flex-end'}}>  
                            <TextInput 
                                   
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {(this.state.year).toString()}
                                    keyboardType={'numeric'}
                                    onChangeText = {(year) => this.setState({year})}
                                    multiline={false}
                                />

                        </View> 

                        
                    </View>

                    <View
                        style={{
                            borderBottomColor: 'black',
                            borderBottomWidth: 1,
                            opacity: 0.1,
                            paddingLeft:17,
                            paddingRight:17,
                            
                        }}
                    /> 

                    <View style={styles.monthSpendings}>
                        <TouchableOpacity style={{padding:5,flex:1,flexDirection:'row',alignItems:'center'}} >
                            <Ionicons name="ios-color-palette" style={{fontSize:25,color:'turquoise',width:33}}/>
                            <Text style={{color: '#ff6c5c', fontWeight:'bold'}} >Kilometer :</Text>
                        </TouchableOpacity>

                        <View  style={{flexDirection:'row' ,justifyContent:'flex-end', alignItems:'flex-end'}}>  
                            <TextInput 
                                  
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {(this.state.kilometer).toString()}
                                    keyboardType={'numeric'}
                                    onChangeText = {(kilometer) => this.setState({kilometer})}
                                    multiline={false}
                                />

                        </View> 

                        
                    </View>

                    
                    
                
                
            </View>
            
            <View style={styles.bottomStyle}>

           

            

            

            </View>
                
          
            </View>
            
               
                    
        );
    }
}


const styles = StyleSheet.create({
    inputStyle: {
        // marginTop: '3%',
        fontSize:18,
        alignSelf: 'stretch',
        borderBottomColor:'#ff6c5c',
        padding: 10,
        // marginLeft: 50,
        margin:5,
        marginRight:10,
        borderBottomWidth: 1,   
    
        },
    topStyle: {
        flex:0.60,
        backgroundColor:'#f5f5f5',
        // backgroundColor:'green',
        
    },
    bottomStyle:{
        flex:0.39,
        // backgroundColor:'yellow',
        justifyContent:'center',
        paddingBottom:25
    },
    monthSpendings:{
        backgroundColor:'white',
        alignItems:'center',
        flexDirection:'row',
        padding:15
    },  
    input: {
        backgroundColor: "#ffffff",
        borderWidth: 1,
        borderColor: "grey",
        width: 270,
        fontSize: 19,
      },
      buttonLabel: {
        // borderWidth: 0.5,
        // borderColor: "#d6d7da",
        padding: 10,
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 13,
        fontSize: 25,
      },
      button: {
        backgroundColor: 'white',
        width: "33.333333333%",
      },
      numberKeyboard:{
        
        // paddingTop:50,
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#f5f5f5",
        height: 300,
        flex:1,
         
      },
      numberKeyboard2:{
        
        // paddingTop:50,
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#f5f5f5",
        height: 290,
        flex:1,
         
      }

});



function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(CarEdit);