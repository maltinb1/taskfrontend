import React, { Component } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    Animated,
    TouchableOpacity


} from  'react-native';
import { TextInputMask } from 'react-native-masked-text'

import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import { TextInput } from 'react-native-gesture-handler';
import { CheckBox } from 'react-native-elements';



var isHidden = true;

class DamageDetail extends Component {
    constructor(props) {
		super(props);
		this.state = {
            driverFault: this.props.driverFault, 
            manufacturerFault: this.props.manufacturerFault, 
            autoParts: this.props.autoParts,
            damageDetail: this.props.damageDetail, 
            id: this.props.id
		}
    }

    updateDamagaHistory = async()=>{
        
        let token = 'Token '+this.props.token
        this.setState({loading:true})
        // console.log('tokenx',token)
        try {   
            fetch('http://54.203.20.237:5000/Cars/CarDamageHistory/'+this.props.id+'/', {
                method: 'PUT',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
            body: JSON.stringify({
                driverFault: this.state.driverFault, 
                manufacturerFault: this.state.manufacturerFault, 
                autoParts: this.state.autoParts,
                damageDetail: this.state.damageDetail, 
                id: this.state.id
            }),
            }).then((response) => response.json())
                .then((responseJson) => {            
                    console.log('responseJson',responseJson);
                    Actions.CarDetail({carId:this.props.carId});
                })
                .catch((error) => {
                   
                    console.log(error);
                });
            }catch(err) {
                console.log(err); 
            }   
           
  
  }
  


   


    render(){
        let {damageId} = this.props
        return(
            <SafeAreaView style={{flex:1,backgroundColor:'white'}}>
                <View style={styles.headerStyle}>
                    <View style={{flexDirection:'row',flex:1,justifyContent:'center',alignItems:'center'}}>

                        <View style={{flex:1,justifyContent: "center",alignItems: "flex-start"}}>
                            <Text style={{paddingLeft: 10,fontSize:18,color:'#ff6c5c'}} onPress={ Actions.pop}>Cancel</Text> 
                        </View>
                        <Text style={{fontSize: 23, color:'#ff6c5c', fontWeight:'bold'}}>Damage Details</Text>
                    
                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-end"}} onPress={()=> {this.updateDamagaHistory();}}>
                                <Text style={{paddingRight: 10,fontSize:18,color:'#ff6c5c'}}>Update</Text> 
                        </TouchableOpacity>
                        
                    </View>

                </View>

                <View style={styles.topStyle}>

                  <View stlye={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'column'}}>

                       <CheckBox
                        // style={styles.inputStyle}
                        title="Manufacturer Fault"
                        checked={this.state.manufacturerFault}
                        onPress={() => this.setState({ manufacturerFault: !this.state.manufacturerFault })}
                      />

                      <CheckBox
                        // style={styles.inputStyle}
                        title="Driver Fault"
                        checked={this.state.driverFault}
                        onPress={() => this.setState({ driverFault: !this.state.driverFault })}
                      />    

                      <TextInput 
                          placeholder= "Auto Part"
                          autoCorrect={false}
                          style={styles.inputStyle}
                          value = {this.state.autoParts}
                          onChangeText = {(autoParts) => this.setState({autoParts})}
                          multiline={false}
                      />

                      <TextInput 
                          placeholder= "Damage Detail"
                          autoCorrect={false}
                          style={styles.inputStyle}
                          value = {this.state.damageDetail}
                          onChangeText = {(damageDetail) => this.setState({damageDetail})}
                          multiline={true}
                      />


                         
                  </View>

                <View>
                  
                </View>
                  </View>      

                
            </SafeAreaView>
        );
    }
}


const styles= StyleSheet.create({
    inputStyle: {
        // marginTop: '3%',
        fontSize:18,
        alignSelf: 'stretch',
        borderBottomColor:'#ff6c5c',
        padding: 10,
        // marginLeft: 50,
        margin:5,
        marginRight:10,
        borderBottomWidth: 1,   
    
        },
    
    headerStyle: {
        backgroundColor: 'white',
        // backgroundColor: 'orange',
        // flex:1,
        paddingTop:10,
        flex:0.80,
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    topStyle: {
        flex: 9.2,
        backgroundColor:'#f5f5f5',
        paddingTop:15
    },

    input: {
        backgroundColor: "#ffffff",
        borderWidth: 1,
        borderColor: "grey",
        width: 270,
        fontSize: 19,
      },
   
      
    
});



function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,   
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
      
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(DamageDetail);