import React , { Component } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import AntDesign from 'react-native-vector-icons/AntDesign';



var isHidden = true;
const {width,height} = Dimensions.get('window');


class EngineType extends Component{
    constructor(props) {
		super(props);
		this.state = {
        }
    }

    render(){

        return(
            <View style={{flex:1,backgroundColor:'#f5f5f5'}}>
                <View style={{flex:0.13,paddingTop:30, backgroundColor: "#ff6c5c",alignItems:'center',justifyContent:'center' }}>
                    
                    <View style={{flexDirection: 'row',  width: width}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} onPress={Actions.pop}>
                            <AntDesign name="left"  style={{paddingLeft: 10,fontSize:35}} onPress={Actions.pop}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}/> 
                        </TouchableOpacity>

                        {/* <Text style={{fontSize:24,fontWeight:'600'}}>{budgets[editIndex].name}</Text> */}
                        <Text style={{fontSize:24,fontWeight:'600'}}> Engine Type </Text>

                        <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                 
                         </View>

                    </View>
                </View>


                <View style={{flex:0.87,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>

                    <TouchableOpacity style={{flex:1,width:200,height:200,backgroundColor:'#f58f36',justifyContent:'center',alignItems:'center'}} onPress={()=> Actions.Manufacturer()}>
                        <Text style={{fontSize:20}}>Diesel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{flex:1,width:200,height:200,backgroundColor:'#fcba03',justifyContent:'center',alignItems:'center'}} onPress={()=> Actions.AddCar({manufacturerId:2,engineType:1})}>
                         <Text style={{fontSize:20}}>Gas</Text>
                    </TouchableOpacity>

                </View>

            </View>
        )
    }

}


function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(EngineType);