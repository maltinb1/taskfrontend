import React , { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    SafeAreaView,
    Animated,
    Image,
    ActivityIndicator
} from 'react-native';


import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign  from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import { SwipeListView} from 'react-native-swipe-list-view';
import { Actions } from 'react-native-router-flux';
import * as Progress from 'react-native-progress';
import * as taskActions from '../actions/taskActions';
import { connect } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { thisExpression } from '@babel/types';




const {width,height} = Dimensions.get('window');

class Cars extends Component {

    constructor(props) {
		super(props);
		this.state ={
            dieselCars:[],
            gasCars :[],
            cars:[],
            filterCar:'all',
            showComfirm: false,
            loading:false
        }
    }
 
    async componentDidMount(){
        
            let token = 'Token '+this.props.token
            this.setState({loading:true})
            // console.log('tokenx',token)
            try {
              
                fetch('http://54.203.20.237:5000/Cars/CarApi/', {
                    method: 'GET',
                    headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': token
                    },
                    // body: JSON.stringify({
                    //  username: registerUsername,
                    //  email: registerEmail,
                    //  password: registerPassword,
                    //  password2: registerPassword2,
                    //  first_name: 'metin',
                    //  last_name: 'deneme'
                    // }),
                }).then((response) => response.json())
                    .then((responseJson) => {
                        for (var key in responseJson) {   
                            if(responseJson[key]['engine']['type']['name']=='Gas'){
                                this.setState({ gasCars: [...this.state.gasCars, responseJson[key] ]})
                            }
                            else if(responseJson[key]['engine']['type']['name']=="Diesel"){
                                this.setState({ dieselCars: [...this.state.dieselCars, responseJson[key] ] })

                            }
                        }
                        console.log(this.state);
                        // console.log('response',responseJson[39]['engine']['type']['name']);
                        this.setState({cars:responseJson})
                        
                    })
                    .catch((error) => {
                       
                        console.error(error);
                    });
                }catch(err) {
                    console.warn(err); 
                }
                this.setState({loading:false})

         
    }

    carData = () =>{
        let {filterCar} = this.state;
        if(filterCar=="all"){
            data = this.state.cars;
            return data;
        }
        else if(filterCar=="gas"){
            data = this.state.gasCars;
            return data;

        }
        else if(filterCar=="diesel"){
            data = this.state.dieselCars;
            return data;
        }
    }


    deleteCar = async (id) =>{

            let token = 'Token '+this.props.token
            this.setState({loading:true})
            // console.log('id',id)
            
            let url = 'http://54.203.20.237:5000/Cars/CarApi/'+id.toString()+'/'
            // console.log(url);
            try {
                fetch(url, {
                    method: 'DELETE',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': token
                    },
                }).then(() => {
                    try {
              
                        fetch('http://54.203.20.237:5000/Cars/CarApi/', {
                            method: 'GET',
                            headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': token
                            },
                        }).then((response) => response.json())
                            .then((responseJson) => {
                                console.log('jsonnn',responseJson);
                                for (var key in responseJson) {   
                                    if(responseJson[key]['engine']['type']['name']=='Gas'){
                                        this.setState({ gasCars: [...this.state.gasCars, responseJson[key] ]})
                                    }
                                    else if(responseJson[key]['engine']['type']['name']=="Diesel"){
                                        this.setState({ dieselCars: [...this.state.dieselCars, responseJson[key] ] })
        
                                    }
                                }
                                console.log(this.state);
                                this.setState({cars:responseJson})
                                
                            })
                            .catch((error) => {
                               
                                console.error(error);
                            });
                        }catch(err) {
                            console.warn(err); 
                        }
                })
                    
                    .catch((error) => {
                        console.log('errorX',error)
                    });
                }catch(err) {
                    console.warn(err); 
                }
                this.setState({loading:false})

        
    }

    getUpdateItem = async(id)=>{
        let token = 'Token '+this.props.token
        try {
          
            fetch('http://54.203.20.237:5000/Cars/CarApi/'+id+'/', {
                method: 'GET',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
                },
                
            }).then((response) => response.json())
                .then((responseJson) => {
                    
                    Actions.CarEdit({ 
                        price: responseJson['price'], 
                        color: responseJson['color'], 
                        model: responseJson['model'],
                        year: responseJson['yearOfFabrication'],
                        kilometer: responseJson['currentKm'],
                        updateId: responseJson['id'],
                    });
                })
                .catch((error) => {
                    console.error(error);
                });
            }catch(err) {
                console.warn(err); 
    }
}

  
    logout = async () =>{
        let token = 'Token '+this.props.token
        fetch('http://54.203.20.237:5000/auth/logout/', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token
            },
          }).catch((error) => {
                console.error(error);
              });
      }

      

      

    render(){
        
        return(
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                
            <View style={styles.containerStyle}>

                <View style={styles.headerStyle}>

                    <View style={{flex: 1,flexDirection:'row'}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}}   onPress={()=> Actions.AddManufacturer()}>
                            <SimpleLineIcons name="rocket"  style={{paddingLeft: 10,fontSize:25,color:'#ff6c5c'}}/> 
                        </TouchableOpacity>

                        <Text style={{fontSize: 27, color:'#ff6c5c', fontWeight:'bold'}}>Cars</Text>
                        
                        {/* <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-end",paddingRight:10}} onPress={()=>{this.logout(); this.props.deleteToken(''); Actions.Home();  }}>
                            <SimpleLineIcons name="logout"  style={{paddingLeft: 10,fontSize:25,color:'#ff6c5c'}}/> 
                        </TouchableOpacity> */}
                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-end",paddingRight:10}} onPress={()=>Actions.Settings()}>
                            <SimpleLineIcons name="settings"  style={{paddingLeft: 10,fontSize:25,color:'#ff6c5c'}}/> 
                        </TouchableOpacity>
                    </View>
   
                </View>
                <View style={{flex:0.8}}>
                 {this.state.loading ?  <ActivityIndicator color="black" size="large"/> : <View></View>}

                    <View style={{flex:1,flexDirection:'row'}}>
                        <TouchableOpacity style={{flex:0.33,backgroundColor:'#ff6c5c',alignItems:'center',justifyContent:'center'}} onPress={()=> this.setState({filterCar: "all" })}>
                            {this.state.filterCar=='all' ? <Text style={{color:'white',fontWeight: 'bold',underline: {textDecorationLine: 'underline'}, italic: {fontStyle: 'italic'},fontSize: 20,textDecorationLine: 'underline'}}>All</Text> :  <Text style={{color:'white'}}>All</Text>}
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:0.33,backgroundColor:'#75d1ea',alignItems:'center',justifyContent:'center'}} onPress={()=> this.setState({filterCar: "diesel" })}>
                            {this.state.filterCar=='diesel' ? <Text style={{color:'white',fontWeight: 'bold',underline: {textDecorationLine: 'underline'}, italic: {fontStyle: 'italic'},fontSize: 20,textDecorationLine: 'underline'}}>Diesel</Text>: <Text style={{color:'white'}}>Diesel</Text>}
                            {/* <Text style={{color:'white'}}>Diesel</Text> */}
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:0.33,backgroundColor:'#b8bacc',alignItems:'center',justifyContent:'center'}} onPress={()=> this.setState({filterCar: "gas" })}>
                             {this.state.filterCar=='gas' ? <Text style={{color:'white',fontWeight: 'bold',underline: {textDecorationLine: 'underline'}, italic: {fontStyle: 'italic'},fontSize: 20,textDecorationLine: 'underline'}}>Gas</Text>:<Text style={{color:'white'}}>Gas</Text>}
                        </TouchableOpacity>
                    </View>
                </View>
                
                <View style={styles.middleContainerStyle}>
                

                    <View
                    style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                        opacity: 0.1,
                        marginLeft:17,
                        marginRight:17,
                        // paddingTop:10,
                    }}
                    />   

                    {/* <View style={{backgroundColor:'#f5f5f5', flexDirection:'row',height:38,alignItems:'center'}}>
                        <Text style={{paddingLeft:20, fontSize: 13,color:'#808080'}}>Total Amount Left </Text>  
                        
                        <View style={{ flex:1, alignItems:'flex-end'}}>
                            <Text style={{paddingRight:15,color:'#808080'}}>{this.formatNumber((((totalAmountLeft)/100).toFixed(2)))}</Text>         
                        </View>            
                    </View> */}

                    <View
                    style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                        opacity: 0.1,
                        marginLeft:17,
                        marginRight:17,
                    }}
                    />  

                    <ScrollView>


                    <SwipeListView
                            data={this.carData()}
                            keyExtractor={(rowData, index) => {
                                return rowData.id.toString();
                            }}
                            renderItem={ (data, rowMap) => (
                                    <TouchableOpacity activeOpacity={1} style={[styles.standaloneRowFront,{backgroundColor: 'orange'}]} onPress={()=>Actions.CarDetail({carId:data.item.id})}>
                                        <View>
                                            
                                            <FontAwesome name="automobile" color={'#ff6c5c'} size={30} style={{width:37}}/>
                                        </View>

                                        <View style={{flexDirection:'column',paddingLeft: 5 }}>
                                            <Text style={{ fontSize:17, fontWeight:'600',opacity:0.5}}>{data.item.model} {data.item.name}</Text>
                                                 
                                            <Text style={{ fontSize:12, fontWeight:'600',opacity:0.5}}>{data.item.yearOfFabrication}, {data.item.color}, {data.item.currentKm} KM</Text>
                                        </View>

                                        {/* <View style={{backgroundColor: data.item.color,padding:3,borderRadius:10}}> */}
                                            {/* <Text style={{ fontSize:10, fontWeight:'600',opacity:0.5}}>% { ((data.item.amountLeft/data.item.amount)*100).toFixed(2)}</Text> */}
                                        {/* </View> */}

                                        <View style={{flex:1,alignItems:'flex-end',paddingRight:15}}>
                                            <Text style={{fontSize:17, fontWeight:'600',opacity:0.5}}>{data.item.price}$</Text>
                                        </View>
                                        
                                        <ConfirmDialog
                                            title={"Car Delete"}
                                            message= "Every record related to this car will be deleted."
                                            visible={this.state.showComfirm}
                                            onTouchOutside={() => this.setState({dialogVisible: false})}
                                            positiveButton={{
                                            title: "YES",
                                            onPress: () => {this.deleteCar(this.state.rowId); this.setState({showComfirm:false}); }
                                            }}
                                            negativeButton={{
                                            title: "NO",
                                            onPress: () => this.setState({showComfirm:false})
                                        }}
                                        />
                                    
                                    </TouchableOpacity>

                                     
                               

                            )}
                            renderHiddenItem={ (data, rowMap) => (
                            <View style={styles.standaloneRowBack}>
                                <AntDesign name='edit' style={styles.backTextWhite} onPress={()=> this.getUpdateItem(data.item.id)} />
                                <MaterialCommunityIcons name='delete-forever' style={styles.backTextRed} onPress={()=> { this.setState({showComfirm: true,rowId:data.item.id});}}/>
                            </View>

                            )}
                            rightOpenValue={-150} disableRightSwipe={true}
                        />

                        {this.state.cars.length>0 ?  <Text style={{paddingTop:10,textAlign:'center',opacity: 0.5, fontSize:12}}> {'<--'} Swipe left to update and delete car or press on it.</Text> : <Text></Text>}
                       
                 
                
                    </ScrollView>

                </View>
                <View style={styles.endContainerStyle}>

                    <TouchableOpacity style={{flexDirection: 'row',paddingLeft:40,flex:1,justifyContent:"flex-start",alignItems:'center'}} onPress={()=> Actions.EngineType()}>                               
                        <Text style={{fontSize:20, color:'white'}}>Add New Car</Text>
                        <Entypo name='add-to-list' style={{flex:1,fontSize:30,textAlign:'right',paddingRight: 30,color:'white'}} />
                    </TouchableOpacity>  
                </View>                            

             </View>


            </SafeAreaView>


    
            
        );
        
    
    
    }
    
    
    
}



const styles = StyleSheet.create({
    standaloneRowFront: {
        paddingLeft: 20,
		alignItems: 'center',
        backgroundColor: '#75d1ea',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.4,
        // justifyContent: 'center',
        flex:1,
        height: 80,
        flexDirection:'row'
	},
	standaloneRowBack: {
		alignItems: 'center',
		backgroundColor: 'white',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		padding: 15
    },
    backTextWhite: {
        fontSize: 28,
        color: '#05b074',
        paddingLeft:30,
        paddingRight: 10,
        // justifyContent:'space-evenly'
    },
    backTextRed: {
        fontSize: 28,
        color: '#ff6c5c',
        paddingLeft:30,
        paddingRight: 10,
        // justifyContent:'space-evenly'
	},
    containerStyle: {
        backgroundColor: 'white',
        flex:1
    },
    headerStyle: {
        backgroundColor: 'white',
        // backgroundColor: 'orange',
        // flex:1,
        paddingTop:10,
        flex:0.65,
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    tabBarStyle:{
        
        flex:3,
        paddingLeft: 15,
        paddingRight:15,
        flexDirection:'row',
        width: width,
        alignItems:'center',
        justifyContent:'space-evenly'
    },
    budgetListItemStyle:{
        height:55,
        backgroundColor:'#75d1ea'
    },
    budgetListItemStyle2:{
        height:55,
        backgroundColor:'#74ea9f'
    },  
    budgetListItemStyle3:{
        height:55,
        backgroundColor:'#df5040'
    },    
    middleContainerStyle:{
        flex:9,
        // backgroundColor:'yellow'

    },
    endContainerStyle:{
        // marginBottom: height/24,
        backgroundColor:'#ff6c5c',
        flex: 1.
    },
    subView: {
        paddingTop:50,
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        // backgroundColor: "#75d1ea",
        backgroundColor: "#3b77f0",
        height: height,
        flex:1
      },
      
      
});


function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Cars);