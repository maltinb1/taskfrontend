import React , { Component } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';





var isHidden = true;
const {width,height} = Dimensions.get('window');


class Settings extends Component{
    constructor(props) {
		super(props);
		this.state = {
        }
    }

    logout = async () =>{
        let token = 'Token '+this.props.token
        fetch('http://54.203.20.237:5000/auth/logout/', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token
            },
          }).catch((error) => {
                console.error(error);
              });
      }

    render(){

        return(
            <View style={{flex:1,backgroundColor:'#f5f5f5'}}>
                <View style={{flex:0.13,paddingTop:30, backgroundColor: "#ff6c5c",alignItems:'center',justifyContent:'center' }}>
                    
                    <View style={{flexDirection: 'row',  width: width}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} onPress={Actions.pop}>
                            <AntDesign name="left"  style={{paddingLeft: 10,fontSize:35}} onPress={Actions.pop}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}/> 
                        </TouchableOpacity>

                        {/* <Text style={{fontSize:24,fontWeight:'600'}}>{budgets[editIndex].name}</Text> */}
                        <Text style={{fontSize:24,fontWeight:'600'}}> Settings </Text>

                        <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                 
                         </View>

                    </View>
                </View>


                <View style={{flex:0.87,flexDirection:'column'}}>

                    <TouchableOpacity style={{height:70,backgroundColor:'#f58f36',flexDirection:'row',alignItems:'center', shadowOpacity: 0.2,shadowOffset: { width: 0, height: 1 },}} onPress={()=> Actions.PasswordChange()}>
                        <FontAwesome name="user-secret" style={{paddingLeft: 10,fontSize:20,color:'white'}}/> 
                        <Text style={{paddingLeft: 10,fontSize:20,color:'white'}}>Change Password</Text>
                    </TouchableOpacity>

                     <TouchableOpacity style={{height:70,backgroundColor:'#f58f36',flexDirection:'row',alignItems:'center', shadowOpacity: 0.2,shadowOffset: { width: 0, height: 1 },}} onPress={()=>{this.logout(); this.props.deleteToken(''); Actions.Home();  }}>
                            <SimpleLineIcons name="logout" style={{paddingLeft: 10,fontSize:20,color:'white'}}/> 
                            <Text style={{paddingLeft: 10,fontSize:20,color:'white'}}>Logout</Text>
                    </TouchableOpacity>

                   

                </View>

            </View>
        )
    }

}


function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Settings);