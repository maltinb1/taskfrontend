import React , { Component } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    Alert
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { ConfirmDialog } from 'react-native-simple-dialogs';




var isHidden = true;
const {width,height} = Dimensions.get('window');


class PasswordChange extends Component{
    constructor(props) {
		super(props);
		this.state = {
            old_password:'',
            new_password:'',
            loading:false
        }
    }

    _twoOptionAlertHandler=()=>{
        //function to make two option alert
        Alert.alert(
          //title
          'Password changed!',
          //body
          'You will be redirected to home page. You can login with your new password',
          [
            {text: 'Redirect', onPress: () => {this.logout(); this.props.deleteToken(''); Actions.Home();}},
            // {text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel'},
          ],
          { cancelable: false }
          //clicking out side of alert will not cancel
        );
      }

    logout = async () =>{
        let token = 'Token '+this.props.token
        fetch('http://54.203.20.237:5000/auth/logout/', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token
            },
          }).catch((error) => {
                console.error(error);
              });
      }

    passwordChange = async () =>{
        let token = 'Token '+this.props.token
        fetch('http://54.203.20.237:5000/auth/password_change/', {
            method: 'PUT',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token
            },
            body: JSON.stringify({
                    old_password: this.state.old_password,
                    new_password: this.state.new_password
                }),
            }).then((response) => response.json())
            .then((responseJson) => {
                if(responseJson['password']=='changed'){
                    this.setState({passwordChanged:true});
                    this._twoOptionAlertHandler();
                    
                }
                console.log('responseJson',responseJson);
                this.setState({newPasswordValidationErrors: responseJson['new_password']})
                this.setState({oldPasswordValidationErrors: responseJson['old_password']})
                this.setState({loading:false});
            }).catch((error) => {
                this.setState({loading:false});
                console.log(error);
              });
              
      }

      continueButton = ()=>{
        if(this.state.old_password=='' || this.state.new_password==''){
          return(
            <TouchableOpacity  disabled={true} style={{flex:0.1,justifyContent:"center",alignItems:'center',backgroundColor:'#ff6c5c'}}  >
                <Text style={{fontWeight:'600', fontSize:24,opacity:0.3,color:'white'}}>Change Password</Text>
            </TouchableOpacity>
          )
        }
        else{
            return(
              <TouchableOpacity style={{flex:0.1,justifyContent:"center",alignItems:'center',backgroundColor:'#ff6c5c'}} onPress={()=> {this.passwordChange();   this.setState({loading:true});}} >
                 <Text style={{fontWeight:'600', fontSize:24,color:'white'}}>Change Password</Text>
              </TouchableOpacity>
            )
        }
      }

    render(){

        return(
            <View style={{flex:1,backgroundColor:'#f5f5f5'}}>
                <View style={{flex:0.13,paddingTop:30, backgroundColor: "#ff6c5c",alignItems:'center',justifyContent:'center' }}>
                    
                    <View style={{flexDirection: 'row',  width: width}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} onPress={Actions.pop}>
                            <AntDesign name="left"  style={{paddingLeft: 10,fontSize:35}} onPress={Actions.pop}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}/> 
                        </TouchableOpacity>

                        {/* <Text style={{fontSize:24,fontWeight:'600'}}>{budgets[editIndex].name}</Text> */}
                        <Text style={{fontSize:24,fontWeight:'600'}}> Password Change </Text>

                        <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                 
                         </View>

                    </View>
                </View>
                <View style={{flex:0.87,justifyContent:'center'}}>
                        {this.state.loading ?  <ActivityIndicator color="black" size="large"/> : <View></View>}
                      
                        <TextInput 
                            placeholder= "Old Password"
                            autoCorrect={false}
                            style={styles.inputStyle}
                            secureTextEntry={true} 
                            value = {this.state.old_password}
                            onChangeText = {(old_password) => this.setState({old_password})}
                            multiline={false}
                        />

                        <TextInput 
                            placeholder= "New Password"
                            autoCorrect={false}
                            style={styles.inputStyle}
                            secureTextEntry={true} 
                            value = {this.state.new_password}
                            onChangeText = {(new_password) => this.setState({new_password})}
                            multiline={false}
                        />
                    
                    <View style={{justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
                        <Text>{this.state.oldPasswordValidationErrors}</Text>
                        <Text>{this.state.newPasswordValidationErrors}</Text>
                       
                    </View>
 

                    <View style={{justifyContent:'center', alignItems:'center',paddingTop:20}}>
                            {this.state.passwordSent ? <Text >Password reset e-mail has been sent.</Text>:<Text></Text>}
                    </View>
                        {this.continueButton()}

                        
                </View>

            </View>
        )
    }

}

const styles = {
    
    inputStyle: {
        // marginTop: '3%',
    alignSelf: 'stretch',
    borderBottomColor:'#ff6c5c',
    paddingBottom:10,
    padding: 10,
    marginLeft: 50,
    margin:5,
    marginRight:50,
    borderBottomWidth: 1,   
    
    }
};


function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(PasswordChange);