import React , { Component } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import AntDesign from 'react-native-vector-icons/AntDesign';



var isHidden = true;
const {width,height} = Dimensions.get('window');


class Manufacturer extends Component{
    constructor(props) {
		super(props);
		this.state = {
            manufacturer: '',
            manufacturerList:[]
        }
    }

    ChangeColorFunction = () => {

        var ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
        this.setState({
          ColorHolder: ColorCode
        })
      }

    async componentDidMount(){
        
        let {manufacturer} = this.state;
        let token = 'Token '+this.props.token;
        try{
            fetch('http://54.203.20.237:5000/Cars/ManufacturerApi/', {
            method: 'GET',
            headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
            },
            // body: JSON.stringify({
            //  username: username,
            //  password: password,
            // }),
        }).then((response) => response.json())
            .then((responseJson) => {
                // console.log(responseJson);
                this.setState({manufacturer:responseJson})
                // const manufacturerList = this.state.manufacturer.map(x => ({
                //     label: x.name,
                //     value: x.id
                // }))
                // this.setState({manufacturerList})
            })
            .catch((error) => {
                console.error(error);
            });
        }catch(err){
            // console.log(err);
        }

    }

    render(){

        return(
            <View style={{flex:1,backgroundColor:'#f5f5f5'}}>
                <View style={{flex:0.13,paddingTop:30, backgroundColor: "#ff6c5c",alignItems:'center',justifyContent:'center' }}>
                    
                    <View style={{flexDirection: 'row',  width: width}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} onPress={Actions.pop}>
                            <AntDesign name="left"  style={{paddingLeft: 10,fontSize:35}} onPress={Actions.pop}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}/> 
                        </TouchableOpacity>

                        {/* <Text style={{fontSize:24,fontWeight:'600'}}>{budgets[editIndex].name}</Text> */}
                        <Text style={{fontSize:24,fontWeight:'600'}}> Manufacturer </Text>

                        <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                 
                         </View>

                    </View>
                </View>


                <View style={{flex:0.87,flexDirection:'column'}}>
               
                {this.state.manufacturer=='' ? <View></View> :  
                    <View>
                        {this.state.manufacturer.map(x => (
                                    <View>
                                         {x.name=="Ours" ?  <View></View>:  
                                         
                                    <TouchableOpacity style={{backgroundColor:'#f58f36',shadowOpacity:0.1,height:50,justifyContent:'center'}} onPress={()=> Actions.AddCar({manufacturerId: x.id, engineType: 2 }) }>
                                         <Text style={{fontSize:20,textAlign:'center'}}>{x.name}</Text> 
                                      </TouchableOpacity>}

                                    </View>
                           
                        ))}

                    </View>
                    }
                  

                   

                   

                </View>

                

            </View>
        )
    }

}


function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Manufacturer);