import React , { Component } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    Dimensions,

} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Actions } from 'react-native-router-flux';
import { TextInput } from 'react-native-gesture-handler';


export default class Forgotten extends Component{
    constructor(props) {
		super(props);
		this.state = {
            text:'',
            passwordSent: false
		}
    }

    passwordReset = async()=>{
        try {   
            fetch('http://54.203.20.237:5000/rest-auth/password/reset/', {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                 email: this.state.email,
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    console.log('resonseJson',responseJson);
                    this.setState({passwordSent:true})
                })
                .catch((error) => {
                   
                    console.log('error',error);
                });
            }catch(err) {
                console.log('err',err);
            }
        
    }

    continueButton = ()=>{
        if(this.state.email==''){
          return(
            <TouchableOpacity  disabled={true} style={{marginTop:50,flex:0.2,justifyContent:"center",alignItems:'center',backgroundColor:'#ff6c5c'}}  >
                <Text style={{fontWeight:'600', fontSize:24,opacity:0.3,color:'white'}}>Recover Password</Text>
            </TouchableOpacity>
          )
        }
        else{
            return(
              <TouchableOpacity style={{marginTop:50,flex:0.2,justifyContent:"center",alignItems:'center',backgroundColor:'#ff6c5c'}} onPress={()=> this.passwordReset()} >
                 <Text style={{fontWeight:'600', fontSize:24,color:'white'}}>Recover Password</Text>
              </TouchableOpacity>
            )
        }
      }

    render(){
        const {width,height} = Dimensions.get('window');
        return(
            <SafeAreaView style={{flex:1,backgroundColor:'white'}}>
                <View style={{flex:0.15,flexDirection: 'row',  width: width,paddingTop:30}}>

                    <View style={{flex:1,justifyContent: "center",alignItems: "flex-start"}}>
                        <AntDesign name="left"  style={{paddingLeft: 10,fontSize:35}} onPress={Actions.pop}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}/> 
                    </View>
                 
                    <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>

                     </View>
                </View>

                <View style={{flex:0.75}}>
                        <View style={{flexDirection:'column'}}>
                            <Text style={{paddingLeft:20,fontSize:30,paddingBottom:10}}>Forgot Password</Text>
                            <Text style={{paddingLeft:20,opacity:0.7,fontSize:18,paddingBottom:40}}>Please enter your registered email to reset your password</Text>
                        </View>
                            
                        <TextInput 
                            placeholder= "Email"
                            autoCorrect={false}
                            style={styles.inputStyle}
                            value = {this.state.email}
                            onChangeText = {(email) => this.setState({email})}
                            multiline={false}
                        />

                        <View style={{justifyContent:'center', alignItems:'center',paddingTop:20}}>
                             {this.state.passwordSent ? <Text >Password reset e-mail has been sent.</Text>:<Text></Text>}
                        </View>
                        {this.continueButton()}

                        
                </View>

            </SafeAreaView>
        )
    }
}

const styles = {
    
    inputStyle: {
        // marginTop: '3%',
    alignSelf: 'stretch',
    borderBottomColor:'#ff6c5c',
    paddingBottom:10,
    padding: 10,
    marginLeft: 50,
    margin:5,
    marginRight:50,
    borderBottomWidth: 1,   
    
    }
};
