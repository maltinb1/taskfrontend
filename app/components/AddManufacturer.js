import React , { Component } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import AntDesign from 'react-native-vector-icons/AntDesign';



var isHidden = true;
const {width,height} = Dimensions.get('window');


class AddManufacturer extends Component{
    constructor(props) {
		super(props);
		this.state = {
            name:''
        }
    }

    addManufacturer = async()=>{
        let token = 'Token '+this.props.token
        try {
          
            fetch('http://54.203.20.237:5000/Cars/ManufacturerApi/', {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
                },
            body: JSON.stringify({
               name: this.state.name
            }),
                
            }).then((response) => response.json())
                .then((responseJson) => {
                    console.log('response',responseJson);
                
                })
                .catch((error) => {
                    
                    console.error(error);
                });
            }catch(err) {
                console.warn(err); 
    }
    }

    isBlank = ()=>{
        if (this.state.name=='' ) {
              return(
                <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                    <Text style={{fontSize:18,fontWeight:'400', paddingRight:10,opacity:0.3}}>Save</Text> 
                </View>
              )
        }
        else{
            return (
                <TouchableOpacity  style={{flex:1,justifyContent: "center",alignItems: "flex-end"}} onPress={()=> {this.addManufacturer(); Actions.Cars();}}>
                    <Text style={{fontSize:18,fontWeight:'400', paddingRight:10}} >Save</Text> 
                </TouchableOpacity>
            )
        }
    }

    render(){

        return(
            <View style={{flex:1,backgroundColor:'#f5f5f5'}}>
                <View style={{flex:0.13,paddingTop:30, backgroundColor: "#ff6c5c",alignItems:'center',justifyContent:'center' }}>
                    
                    <View style={{flexDirection: 'row',  width: width}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} onPress={Actions.pop}>
                            <AntDesign name="left"  style={{paddingLeft: 10,fontSize:24}} onPress={Actions.pop}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}/> 
                        </TouchableOpacity>

                        {/* <Text style={{fontSize:24,fontWeight:'600'}}>{budgets[editIndex].name}</Text> */}
                        <Text style={{fontSize:24,fontWeight:'600'}}> Add Manufacturer </Text>

                        {/* <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                             <Text   style={{paddingRight: 10,fontSize:20}} onPress={Actions.pop}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}> Save </Text>
                         </View> */}
                         {this.isBlank()}

                    </View>
                </View>


                <View style={{flex:0.87,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>

                    <View style={styles.monthSpendings}>
                        <View style={{padding:5,flex:1,flexDirection:'row',alignItems:'center',padding:6}}>
                                <FontAwesome name="search" style={{fontSize:25,color:'turquoise',width:33}}/>
                                <Text style={{color:'#ff6c5c', fontWeight:'bold'}}>Name :</Text>
                        </View>
                        <View  style={{flexDirection:'row' ,justifyContent:'flex-end', alignItems:'flex-end'}}>  
                            <TextInput 
                                    placeholder= "Enter a Name"
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {this.state.name}
                                    onChangeText = {(name) => this.setState({name})}
                                    multiline={false}
                                />

                        </View> 

                    </View>

                </View>

            </View>
        )
    }

}
const styles = StyleSheet.create({
    monthSpendings:{
        backgroundColor:'white',
        alignItems:'center',
        flexDirection:'row',
        padding:15
    },  
    });

function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddManufacturer);