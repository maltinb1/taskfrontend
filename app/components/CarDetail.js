import React , { Component } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    Text,
    Dimensions,
    TouchableOpacity,
    Animated,
    ActivityIndicator,
    ScrollView,
    TouchableHighlight,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import * as taskActions from '../actions/taskActions';
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import { TextInput } from 'react-native-gesture-handler';
import { CheckBox } from 'react-native-elements';





const {width,height} = Dimensions.get('window');
var isHidden = true;


class CarDetail extends Component{
    constructor(props) {
		super(props);
      this.state = {
            manufacturerFault:false,
            driverFault:false,
            bounceValue: new Animated.Value(height), 
            loading:false,
           

      }
    }

    monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    monthNamesShorter = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


    getUpdateItem = async(id)=>{
      let token = 'Token '+this.props.token
      try {
        
          fetch('http://54.203.20.237:5000/Cars/CarDamageHistory/'+id+'/', {
              method: 'GET',
              headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token
              },
              
          }).then((response) => response.json())
              .then((responseJson) => {
                  
                  Actions.DamageDetail({ 
                      id: responseJson['id'],
                      driverFault: responseJson['driverFault'], 
                      manufacturerFault: responseJson['manufacturerFault'], 
                      autoParts: responseJson['autoParts'],
                      damageDetail: responseJson['damageDetail'],
                      carId: responseJson['car'],               
                  });
              })
              .catch((error) => {
                  console.error(error);
              });
          }catch(err) {
              console.warn(err); 
  }
}


    addNewDamageHistory = async()=>{
      let token = 'Token '+this.props.token
        try{
          fetch('http://54.203.20.237:5000/Cars/CarDamageHistory/', {
              method: 'POST',
              headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token
              },
            body: JSON.stringify({
                 driverFault: this.state.driverFault,
                 manufacturerFault: this.state.manufacturerFault,
                 autoParts: this.state.autoPart,
                 damageDetail: this.state.damageDetail,
                 car: this.props.carId
                }),
          }).then((response) => response.json())
              .then((responseJson) => {            
                  console.log('responseJsonYYYY',responseJson);

                  try {   
                    fetch('http://54.203.20.237:5000/Cars/CarApi/'+this.props.carId+'/', {
                        method: 'GET',
                        headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': token
                        },
                    }).then((response) => response.json())
                        .then((responseJson) => {            
                            console.log('responseJsonXXXX',responseJson);
                            this.setState({carDetail:responseJson})   
                            
                            try{
                              fetch('http://54.203.20.237:5000/Cars/CarDamageHistory/?car='+responseJson['id']+'/', {
                                method: 'GET',
                                headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': token
                                },
                                }).then((response) => response.json())
                                    .then((responseJsonDamage) => {            
                                        console.log('responseJsonDamage',responseJsonDamage);
                                        this.setState({carDamageHistory:responseJsonDamage})           
                                    })
                                    .catch((error) => {
                                        console.log('selam',error);
                                    });
                                }catch(err) {
                                  console.log('selam2',err); 
                              }   
          
                        })
                        .catch((error) => {
                           
                            console.log('selam3',error);
                        });
                    }catch(err) {
                        console.log('selam4',err); 
                    }   





              })
              .catch((error) => {
                 
                  console.log('naber',error);
              });
        }catch(err){
            console.log('heyyo',err)
        }
    }

    isBlank = ()=>{
      if (this.state.autoPart=='' || this.state.damageDetail=='') {
            return(
              <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-end"}} hitSlop={{top: 15, bottom: 15, left: 15, right: 15}}>
                <Text style={{color:'#ff6c5c',fontWeight:'600',paddingRight:15,fontSize:16,opacity: 0.5}}>Done</Text>
              </TouchableOpacity>
  
            )
      }
      else{
          return (
            <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-end"}} hitSlop={{top: 15, bottom: 15, left: 15, right: 15}} onPress={()=> {this.addNewDamageHistory(); this._toggleSubview();}}>
              <Text style={{color:'#ff6c5c',fontWeight:'600',paddingRight:15,fontSize:16}} >Done</Text>
            </TouchableOpacity>
          )
      }
  }


    _toggleSubview() {    
      this.setState({
        buttonText: !isHidden ? "+" : "-" , showPagination: false
      });
  
      var toValue = height;
  
      if(isHidden) {
        toValue = 0;
      }
  
      //This will animate the transalteY of the subview between 0 & 100 depending on its current state
      //100 comes from the style below, which is the height of the subview.
      Animated.spring(
        this.state.bounceValue,
        {
          toValue: toValue,
          velocity: 10,
          tension: 20,
          friction: 7,
        }
      ).start();
      
      isHidden = !isHidden;
    
    }

    async componentDidMount(){
        
      let token = 'Token '+this.props.token
      this.setState({loading:true})
      // console.log('tokenx',token)
      try {   
          fetch('http://54.203.20.237:5000/Cars/CarApi/'+this.props.carId+'/', {
              method: 'GET',
              headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token
              },
          }).then((response) => response.json())
              .then((responseJson) => {            
                  console.log('responseJson',responseJson);
                  console.log('responseJsonId',responseJson['id']);
                  this.setState({carDetail:responseJson})   
                  
                  try{
                    fetch('http://54.203.20.237:5000/Cars/CarDamageHistory/?car_id='+responseJson['id'], {
                      method: 'GET',
                      headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': token
                      },
                      }).then((response) => response.json())
                          .then((responseJsonDamage) => {            
                              console.log('responseJsonDamage',responseJsonDamage);
                              this.setState({carDamageHistory:responseJsonDamage})           
                          })
                          .catch((error) => {
                              console.log(error);
                          });
                      }catch(err) {
                        console.log(err); 
                    }   

              })
              .catch((error) => {
                 
                  console.log(error);
              });
          }catch(err) {
              console.log(err); 
          }   
         

}


    render(){
      let {carDetail,carDamageHistory} = this.state;
      console.log('cardId',this.props.carId)
        if (!this.state.carDamageHistory){
          return(
            <View style={{flex:1,backgroundColor:'#f5f5f5',justifyContent:'center',alignItems:'center'}}> 
                 <ActivityIndicator color="black" size="large"/>
            </View>
          )
        }
        else{
        return(
          

          <View style={{flex:1,backgroundColor:'#f5f5f5'}}>
              <View style={{flex:0.13,paddingTop:30, backgroundColor: "#ff6c5c",alignItems:'center',justifyContent:'center' }}>
                    
                    <View style={{flexDirection: 'row',  width: width}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} onPress={Actions.pop} hitSlop={{top: 15, bottom: 15, left: 15, right: 15}}>
                            <AntDesign name="left"  style={{paddingLeft: 10,fontSize:35}}  /> 
                        </TouchableOpacity>

                        <View style={{flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:24,fontWeight:'600'}}> {carDetail['yearOfFabrication']} {carDetail['model']}</Text>
                          <Text style={{fontSize:17}}> {carDetail['currentKm']} Km </Text>
                        </View>


                        <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                 
                         </View>

                    </View>
              </View>

              <View style={{flex:0.87,flexDirection:'column'}}>

              <ScrollView style={{flex:1}}>
                    <View style={styles.historyStyle}>
              

                        <SwipeListView
                                 
                                data={carDamageHistory}
                                keyExtractor={(rowData, index) => {
                                  return rowData.id.toString();
                                }}
                                renderItem={ (data, rowMap) => (
                                
                                <TouchableOpacity style={styles.historyStyleDetail} onPress={()=> this.getUpdateItem(data.item.id)}> 
 
                                    <View style={{ width:50 , height:50,borderRadius: 100, backgroundColor:'#ff6c5c',alignItems:'center',justifyContent:'center',borderWidth:2,borderColor:'#75d1ea'}}>
                                      <Text style={{color:'white'}}>{parseInt(((data.item.created_at).split('T')[0]).split('-')[2] ) }</Text>
                                    </View>
                                    
                                    <View style={{flex:1,flexDirection:'column'}}>
                                      <Text style={{paddingLeft:20,flex:1,fontSize:0.045*width,fontWeight:'600', opacity:0.5}}>{data.item.autoParts}</Text>
                                      <Text numberOfLines={1} style={{paddingLeft:20,flex:1,fontSize:0.035*width}}>{data.item.damageDetail}</Text>

                                    </View>

                                    <View style={{backgroundColor: '#edecf1',padding:3,borderRadius:10}}>
                                      <Text style={{justifyContent: "center",alignItems:'flex-start',fontSize:0.043*width,color:'green'}}>{ this.monthNames[parseInt(((data.item.created_at).split('T')[0]).split('-')[1]-1)]      }</Text> 
                                    </View>

                                </TouchableOpacity>

                                )}
                               
                              //   renderHiddenItem={ (data, rowMap) => (
                              // <View style={styles.standaloneRowBack}>
                              //     <AntDesign name='edit' style={styles.backTextWhite}  onPress={()=> console.log(data.index)}/>
                              //     <MaterialCommunityIcons name='delete-forever' style={styles.backTextRed} onPress={()=> this.props.deleteHistory(index,this.props.budgets[index].key,this.props.budgets[index].id,this.props.budgets[index].name,this.props.this.props.this.props.budgets[index].currency,this.props.budgets[index].amount,this.props.budgets[index].amountLeft,this.props.this.props.budgets[index].color,this.props.budgets[index].iconName,data.index) }  />
                              // </View>
                              //   )}
                                disableLeftSwipe={true} disableRightSwipe={true}
                                
                            />
                        {/* {budgets[index].history.length>0 ?  <Text style={{paddingTop:10,textAlign:'center',opacity: 0.5, fontSize:12} }>  You can click on budgets to delete or update</Text> : <View style={{flex:1,alignItems:'center',alignContent:'flex-end',paddingTop:20}}><Text style={{fontSize:18,opacity:0.6,fontWeight:'600'}}> Empty here :(</Text></View>} */}
                        
                        

                        </View>
                        
                        
                    
                </ScrollView> 

               
                    
                {/* {budgets[index].history.length>0 ?  <Text> </Text> :<View style={{alignItems:'center',alignContent:'flex-end',paddingBottom:20}}><Text style={{fontSize:18,opacity:0.6}}> You can add your first record now </Text></View>} */}

                <View style={{justifyContent:'flex-end',alignItems:'center',padding:10}}>
                        <TouchableHighlight  style={{backgroundColor:'#ff6c5c',width:50 , height:50,borderRadius: 100,justifyContent:'center',alignItems:'center'}} hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} onPress={()=> {this._toggleSubview()}}>
                            <Text style={{fontSize:25}} >+</Text>
                        </TouchableHighlight>
                </View>
                   

                </View>

                            
            <Animated.View
            style={[styles.subView,
              {transform: [{translateY: this.state.bounceValue}]}]}
            >   
 
               <View style={{flex:0.4}}>
                    <View style={{padding:5,flexDirection:'row'}}>

                        <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-start"}} hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} onPress={()=> {this.setState({showPagination:true}); this._toggleSubview(); this.setState({showPagination: true});}}>
                            <Text  style={{paddingLeft:15,color:'#ff6c5c',fontWeight:'600',fontSize:16}} >Cancel</Text>{/* <MaterialCommunityIcons name="arrow-left-thick"  style={{paddingLeft: 10,fontSize:25}}/>  */}
                        </TouchableOpacity>

                        <Text style={{flex:2,color:'#ff6c5c',fontSize:19,fontWeight:'bold',textAlign:'center'}}>Damage Record</Text>
                                            
                            {this.isBlank()}
                        
                    </View>
                </View>


                  <View stlye={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'column'}}>

                      

                       <CheckBox
                        // style={styles.inputStyle}
                        title="Manufacturer Fault"
                        checked={this.state.manufacturerFault}
                        onPress={() => this.setState({ manufacturerFault: !this.state.manufacturerFault })}
                      />

                      <CheckBox
                        // style={styles.inputStyle}
                        title="Driver Fault"
                        checked={this.state.driverFault}
                        onPress={() => this.setState({ driverFault: !this.state.driverFault })}
                      />    

                      <TextInput 
                          placeholder= "Auto Part"
                          autoCorrect={false}
                          style={styles.inputStyle}
                          value = {this.state.autoPart}
                          onChangeText = {(autoPart) => this.setState({autoPart})}
                          multiline={false}
                      />

                      <TextInput 
                          placeholder= "Damage Detail"
                          autoCorrect={false}
                          style={styles.inputStyle}
                          value = {this.state.damageDetail}
                          onChangeText = {(damageDetail) => this.setState({damageDetail})}
                          multiline={true}
                      />


                         
                  </View>

                <View>
                  
                </View>
              


          </Animated.View>
          
          

          </View>
    
        )
      }
    }
}

const styles = StyleSheet.create({
  inputStyle: {
    // marginTop: '3%',
    fontSize:18,
    alignSelf: 'stretch',
    borderBottomColor:'#ff6c5c',
    padding: 10,
    marginLeft: 50,
    margin:5,
    marginRight:50,
    borderBottomWidth: 1,   

    },
  TopHalfcontainerStyle: {
    flex:1,
    backgroundColor: "#ff6c5c",
},
  BottomHalfContainerStyle: {
    flex:4,
    // backgroundColor: '#4de3cc',
    backgroundColor: 'white',
    flexDirection: 'column'
    // alignSelf:'center', 
    // justifyContent: 'center'
},
    historyStyle:{
        backgroundColor:'white',
        padding:12,
        marginBottom:10,
        flexDirection:'column',
        
    }, 
    historyStyleDetail:{
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row',
        paddingTop:30,
        

    }, 
  standaloneRowFront: {
      paddingLeft: 20,
  alignItems: 'center',
      backgroundColor: '#75d1ea',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.2,
      // justifyContent: 'center',
      flex:1,
      height: 60,
      flexDirection:'row'
},
standaloneRowBack: {
  alignItems: 'center',
  backgroundColor: 'white',
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-end',
  padding: 15
  },
  backTextWhite: {
      fontSize: 28,
      color: '#05b074',
      paddingLeft:30,
      paddingRight: 10,
      // justifyContent:'space-evenly'
  },
  backTextRed: {
      fontSize: 28,
      color: '#ff6c5c',
      paddingLeft:30,
      paddingRight: 10,
      // justifyContent:'space-evenly'
},
  containerStyle: {
      backgroundColor: 'white',
      flex:1
  },
  headerStyle: {
      backgroundColor: 'white',
      // backgroundColor: 'orange',
      // flex:1,
      paddingTop:10,
      flex:0.65,
      flexDirection:'column',
      justifyContent: 'center',
      alignItems: 'center',
      // shadowColor: '#000',
      // shadowOffset: { width: 0, height: 2 },
      // shadowOpacity: 0.2,
      elevation: 2,
      position: 'relative'
  },
  

  middleContainerStyle:{
      flex:9,
      // backgroundColor:'yellow'

  },
  endContainerStyle:{
      // marginBottom: height/24,
      backgroundColor:'#ff6c5c',
      flex: 1.
  },
  subView: {
      paddingTop:50,
      position: "absolute",
      bottom: 0,
      left: 0,
      right: 0,
      // backgroundColor: "#75d1ea",
      backgroundColor: "white",
      height: height,
      flex:1
    },
    
    
});



function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      deleteToken: (token) => dispatch(taskActions.deleteToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(CarDetail);