import React , { Component } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    Dimensions
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { TextInput } from 'react-native-gesture-handler';
import * as taskActions from '../actions/taskActions';
import { connect } from 'react-redux';


const {width,height} = Dimensions.get('window');

class Home extends Component{
    constructor(props) {
		super(props);
		this.state = {
            isLogin: true
		}
    }


  signUp = async () =>{
    let {registerUsername,registerEmail,registerPassword,registerPassword2,first_name,last_name} = this.state;
    // console.log(registerUsername)
    fetch('http://54.203.20.237:5000/ProfileRegisterApi/', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
         username: registerUsername,
         email: registerEmail,
         password: registerPassword,
         password2: registerPassword2,
         first_name: first_name,
         last_name: last_name
        }),
      }).then((response) => response.json())
        .then((responseJson) => {
            console.log('responseJson',responseJson);
            // if(responseJson){
                
            // }
            if(responseJson['token']==undefined){
                console.log('response',responseJson);
                
                this.setState({registerUsernameError: responseJson['username']})
                this.setState({registerEmailError: responseJson['email']})
                this.setState({registerPasswordError: responseJson['password']})
                this.setState({registerPassword2Error: responseJson['password2']})
                this.setState({registerFirstNameError: responseJson['first_name']})
                this.setState({registerLastNameError: responseJson['last_name']})
               
            }
            else{
                this.props.addToken(responseJson['token'])
                Actions.Cars();
            }
            
         
            
        })
          .catch((error) => {
            console.error(error);
          });
  }

  signIn = async () =>{
    let {registerUsername,registerEmail,registerPassword,registerPassword2,username,password} = this.state;
    // console.log(registerUsername)
    fetch('http://54.203.20.237:5000/login/', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
         username: username,
         password: password,
        }),
      }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson['token']==undefined){
                console.log('response',responseJson);
                this.setState({signInErrorMessage: responseJson['error']});
            }
            else{
                this.props.addToken(responseJson['token'])
                Actions.Cars();
            }
        })
          .catch((error) => {
            console.error(error);
          });
  }



  isLogin = () => {
    if (this.state.isLogin==true){
        return(
            <View style={{flex:1,flexDirection:'row'}}>
                <LinearGradient colors={['#5185f5', '#8b61bf', '#b05bb4']} style={{flex:0.5,backgroundColor:'orange',borderBottomRightRadius:100,borderTopRightRadius:100}}>
                    <Text style={{paddingTop:80,paddingLeft:20,fontSize:25,fontWeight:'600',color:'white'}}>Login</Text>
                    
                </LinearGradient>
                  
                 
                <TouchableOpacity  style={{flex:0.5,backgroundColor:'white',borderBottomLeftRadius:100,borderTopLeftRadius:100}} onPress={()=> this.setState({isLogin:false})}>
                        <Text style={{paddingTop:80,paddingRight:20,fontSize:25,fontWeight:'600',color:'grey',textAlign:'right'}}>Sign up</Text>
                </TouchableOpacity>
               
                <View style={{position:'absolute',top:150,left:20,width:width/1.15,height:height/3 ,backgroundColor:'white',borderRadius:30,shadowOpacity:0.5}}>
                        {/* <View style={styles.triangle}>

                        </View> */}
                         <View style={{flex:1,justifyContent:'center'}}>

                                <TextInput 
                                    placeholder= "Username"
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {this.state.username}
                                    onChangeText = {(username) => this.setState({username})}
                                    multiline={false}
                                />
                              
                            
                            <TextInput 
                                placeholder= "Password"
                                autoCorrect={false}
                                style={styles.inputStyle}
                                value = {this.state.password}
                                secureTextEntry={true} 
                                onChangeText = {(password) => this.setState({password})}
                                multiline={false}
                            />
                            <TouchableOpacity style={{paddingTop:30,alignItems:'flex-end',paddingRight:18,paddingBottom:15}} onPress={()=>{Actions.Forgotten()}}>
                                <Text style={{fontWeight:'700',fontSize:16,color:'grey'}} >Forgot Password?</Text>
                            </TouchableOpacity>
                            
                            <View style={{justifyContent:'center',alignItems:'center'}}>
                                <Text style={{color:'red',fontWeight:'600'}}>{this.state.signInErrorMessage}</Text>
                            </View>
                        </View>

                        {/* <View style={{flex:0.2,justifyContent:'flex-end'}}>
                            <SvgUri
                                style={{flex:1}}
                                width="100%"
                                height="70"
                                uri="https://svgshare.com/i/GcX.svg"
                            />
                        </View> */}
                </View>
                

                <TouchableOpacity style={{top:(130+height/3),left:((width/2)-75) ,position:'absolute',backgroundColor:'blue',width:150,height:50,borderRadius:30,justifyContent:'center',shadowOpacity:0.5}} onPress={()=>{this.signIn()}}>
                    <Text style={{textAlign:'center',color:'white',fontSize:18}}>Login</Text>
                </TouchableOpacity>
                
         </View>
         
        )

        
    }
    else {
        return(
        <View style={{flex:1,flexDirection:'row'}}>

            <TouchableOpacity  style={{flex:0.5,backgroundColor:'white',borderBottomRightRadius:100,borderTopRightRadius:100}} onPress={()=> this.setState({isLogin:true})}>
                <Text style={{paddingTop:80,paddingLeft:20,fontSize:25,fontWeight:'600',color:'grey'}}>Login</Text>
            </TouchableOpacity>

            <LinearGradient colors={['#7974da', '#9e56ae', '#e74495']} style={{flex:0.5,backgroundColor:'orange',borderBottomLeftRadius:100,borderTopLeftRadius:100}}>
                <Text style={{paddingTop:80,paddingRight:20,fontSize:25,fontWeight:'600',color:'white',textAlign:'right'}}>Sign up</Text>
            </LinearGradient>
            <View style={{position:'absolute',top:150,left:20,width:width/1.15,height:height/3+90 ,backgroundColor:'white',borderRadius:30,shadowOpacity:0.5}}>
                        {/* <View style={styles.triangle}>

                        </View> */}
                         <View style={{flex:1,justifyContent:'center'}}>

                                <TextInput 
                                    placeholder= "Username"
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {this.state.registerUsername}
                                    onChangeText = {(registerUsername) => this.setState({registerUsername})}
                                    multiline={false}
                                />
                                <View>
                                    <Text style={{ color:'red',marginLeft: 50}}>{this.state.registerUsernameError}</Text>
                                </View>
                              
                            
                            <TextInput 
                                placeholder= "Email"
                                autoCorrect={false}
                                style={styles.inputStyle}
                                value = {this.state.registerEmail}
                                onChangeText = {(registerEmail) => this.setState({registerEmail})}
                                multiline={false}
                            />
                            <View>
                                    <Text style={{color:'red', marginLeft: 50}}>{this.state.registerEmailError}</Text>
                                </View>

                            <TextInput 
                                    placeholder= "Password"
                                    autoCorrect={false}
                                    style={styles.inputStyle}
                                    value = {this.state.registerPassword}
                                    secureTextEntry={true} 
                                    onChangeText = {(registerPassword) => this.setState({registerPassword})}
                                    multiline={false}
                            />
                                 <View>
                                    <Text style={{ color:'red',marginLeft: 50}}>{this.state.registerPasswordError}</Text>
                                </View>
                                                 
                            <TextInput 
                                placeholder= "Confirm Password"
                                autoCorrect={false}
                                style={styles.inputStyle}
                                value = {this.state.registerPassword2}
                                secureTextEntry={true} 
                                onChangeText = {(registerPassword2) => this.setState({registerPassword2})}
                                multiline={false}
                            />
                         <View>
                                    <Text style={{color:'red', marginLeft: 50}}>{this.state.registerPassword2Error}</Text>
                                </View>
                            <TextInput 
                                placeholder= "First Name"
                                autoCorrect={false}
                                style={styles.inputStyle}
                                value = {this.state.first_name}                     
                                onChangeText = {(first_name) => this.setState({first_name})}
                                multiline={false}
                            />
                                 <View>
                                    <Text style={{color:'red', marginLeft: 50}}>{this.state.registerFirstNameError}</Text>
                                </View>
                            <TextInput 
                                placeholder= "Last Name"
                                autoCorrect={false}
                                style={styles.inputStyle}
                                value = {this.state.last_name}     
                                onChangeText = {(last_name) => this.setState({last_name})}
                                multiline={false}
                            />
                                <View>
                                    <Text style={{color:'red', marginLeft: 50}}>{this.state.registerLastNameError}</Text>
                                </View>
                            {/* <TouchableOpacity style={{paddingTop:30,alignItems:'flex-end',paddingRight:18}} onPress={()=>{Actions.Forgotten()}}>
                                <Text style={{fontWeight:'700',fontSize:16,color:'grey'}} >Forgot Password?</Text>
                            </TouchableOpacity> */}
                        </View>

                        {/* <View style={{flex:0.2,justifyContent:'flex-end'}}>
                            <SvgUri
                                style={{flex:1}}
                                width="100%"
                                height="70"
                                uri="https://svgshare.com/i/GcX.svg"
                            />
                        </View> */}
                </View>
                <TouchableOpacity style={{top:(230+height/3),left:((width/2)-75) ,position:'absolute',backgroundColor:'blue',width:150,height:50,borderRadius:30,justifyContent:'center',shadowOpacity:0.5}}  onPress={()=>{this.signUp();}}>
                    <Text style={{textAlign:'center',color:'white',fontSize:18}}>Sign Up</Text>
                </TouchableOpacity>

        </View>
)
    }
  }

    
    render(){
        
        return(
            <SafeAreaView style={{flex:1,backgroundColor:'white'}}>

                <View style={{flex:0.15,flexDirection: 'row',  width: width,paddingTop:30}}>

                    <View style={{flex:1,justifyContent: "center",alignItems: "flex-start"}}>
                        {/* <MaterialCommunityIcons name="arrow-left-thick"  style={{paddingLeft: 10,fontSize:25}}/>  */}
                        {/* <Text style={styles.header}>Login</Text> */}
                    </View>
                    <FontAwesome name="automobile" style={{justifyContent: "center",alignItems: "center", color: '#ff6c5c',fontSize:65}}/> 

                    {/* <Text style={styles.header}>Logo</Text> */}

                    <View style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}>
                    
                    <TouchableOpacity style={{flex:1,justifyContent: "center",alignItems: "flex-end"}}  hitSlop={{top: 50, bottom: 50, left: 50, right: 50}}>
                        
        
                    </TouchableOpacity>
                    </View>

                </View>
                

                <View style={{flex:0.85}}>
                    {this.isLogin()}
                </View>

                {/* <View style={{flex:0.1,justifyContent:'center'}}>
                        <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <Text style={{opacity:0.7,fontSize:16}}>Don't have an account?</Text>
                            <Text style={{color:'#e74b99',fontWeight:'700',fontSize:16}}>  SIGN UP</Text>
                        </View>
                       
                </View> */}

            </SafeAreaView>
        )
    }
}



const styles = StyleSheet.create({
    triangle: {
        position:'absolute',top:-43,left:((width/2)-120),
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 27,
        borderRightWidth: 27,
        borderBottomWidth: 43,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor:"white",
        shadowOpacity:0.5,
        
        margin: 0,
        marginLeft: -6,
        borderWidth: 0,
        borderColor:"black"
    },
  // Header styles
  inputStyle: {
    // marginTop: '3%',
    fontSize:18,
    alignSelf: 'stretch',
    borderBottomColor:'#ff6c5c',
    padding: 5,
    marginLeft: 50,
    margin:5,
    marginRight:50,
    borderBottomWidth: 1,   

    },
  header: {
    // marginTop: '17%',
    
    justifyContent: "center",
    alignItems: "center", 
    color: '#ff6c5c',
    // fontFamily: 'Thonburi-Bold',
    fontSize: 35,
    fontWeight: 'bold',
    marginVertical: 15,
  },

 
});



function mapStateToProps(state) {
    return {
        token: state.TaskReducer.token,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
    //   createBudget: (budgetCreate) => dispatch(taskActions.createBudget(budgetCreate)), 
      addToken: (token) => dispatch(taskActions.addToken(token))
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home);